import 'package:flutter/material.dart';
import 'package:quizzler/question.dart';
import 'package:quizzler/quiz_brain.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

void main() => runApp(const Quizzler());

class Quizzler extends StatelessWidget {
  const Quizzler({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Quizzler',
      home: Scaffold(
        backgroundColor: Colors.grey.shade900,
        body: const SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: QuizPage(),
          ),
        ),
      ),
    );
  }
}

class QuizPage extends StatefulWidget {
  const QuizPage({Key? key}) : super(key: key);

  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  List<Widget> scores = [];

  static final QuizBrain quizBrain = QuizBrain();
  Question? currentQuestion = quizBrain.getQuestion();
  int score = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          flex: 5,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Text(
                currentQuestion?.question ?? 'Quiz Complete!',
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
        TrueButton(() {
          tallyScores(true);
        }),
        FalseButton(() {
          tallyScores(false);
        }),
        Row(
          children: scores,
        )
      ],
    );
  }

  void tallyScores(bool guess) {
    if (currentQuestion == null) {
      setState(() {
        Alert(
          context: context,
          closeFunction: () {
            resetGame();
            Navigator.pop(context);
          },
          title: 'Quizzler',
          desc: 'Quiz Complete!\n Restart quiz?',
          buttons: [
            DialogButton(
              child: const Text('Okay'),
              onPressed: () {
                resetGame();
                Navigator.pop(context);
              },
            ),
          ],
        ).show();
      });
      return;
    }

    setState(() {
      if (guess == currentQuestion!.answer) {
        scores.add(const CorrectIcon());
      } else {
        scores.add(const WrongIcon());
      }
      currentQuestion = quizBrain.getQuestion();
    });
  }

  void resetGame() {
    quizBrain.reset();
    score = 0;

    setState(() {
      currentQuestion = quizBrain.getQuestion();
      scores.clear();
    });
  }
}

class WrongIcon extends StatelessWidget {
  const WrongIcon({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Icon(
      Icons.close,
      color: Colors.red,
    );
  }
}

class CorrectIcon extends StatelessWidget {
  const CorrectIcon({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Icon(
      Icons.check,
      color: Colors.green,
    );
  }
}

class TrueButton extends StatelessWidget {
  TrueButton(
    this.onPressed, {
    Key? key,
  }) : super(key: key);

  void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: TextButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white)),
          onPressed: onPressed,
          child: const Text(
            'True',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20.0,
            ),
          ),
        ),
      ),
    );
  }
}

class FalseButton extends StatelessWidget {
  FalseButton(
    this.onPressed, {
    Key? key,
  }) : super(key: key);

  void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: TextButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
          onPressed: onPressed,
          child: const Text(
            'False',
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}


/*
question1: ,
question2:
*/