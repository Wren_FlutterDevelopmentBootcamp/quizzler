class Question extends Object {
  const Question(this.question, this.answer);

  final String question;
  final bool answer;

  @override
  String toString() {
    return 'Question: $question \n Answer: $answer';
  }
}
